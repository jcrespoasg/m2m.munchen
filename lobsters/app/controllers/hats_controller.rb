require 'hat_request_representer'
require 'faraday'
require 'zipkin-tracer'

class HatsController < ApplicationController
  before_action :require_logged_in_user, :except => [ :index ]
  before_action :require_logged_in_moderator,
    :except => [ :build_request, :index, :create_request ]

  def build_request
    @title = "Request a Hat"

    @hat_request = HatRequest.new
  end

  def index
    @title = "Hats"

    @hat_groups = {}

    Hat.all.includes(:user).each do |h|
      @hat_groups[h.hat] ||= []
      @hat_groups[h.hat].push h
    end
  end

  def create_request
    # Build links we will use as identifiers
    subject = url_for(controller: "users", action: "show", username: @user.username)
    object = url_for(controller: "hat", action: "show", hat: params[:hat_request][:hat])
    
    text_evidence = {:text => params[:hat_request][:comment]}
    linked_evidence = {:link => params[:hat_request][:link]}

    # Make a data structure that will be the body of the API request
    hat_request = HatRequestRepresenter.new(
      :subject => subject,
      :object => object,
      :evidence => [text_evidence, linked_evidence])

    # Look to see if there's an existing span to join
    # span = request.env['rack.span']

    # "Faraday" makes the HTTP call for us. 
    conn = Faraday.new(:url => Lobsters::Application.config.request_service_uri) do |c|
      c.use ZipkinTracer::FaradayHandler
      c.request :url_encoded
      c.adapter Faraday.default_adapter
    end

    # Do the call to the API
    conn.post do |req|
      req.headers['Content-Type'] = 'application/json' 
      req.body = hat_request.to_json
    end

    # Send the UI to the list of all hat wearers
    return redirect_to "/hats"
  end
      
  def requests_index
    @title = "Hat Requests"

    @hat_requests = HatRequest.all.includes(:user)
  end

  def approve_request
    @hat_request = HatRequest.find(params[:id])
    @hat_request.update_attributes!(params.require(:hat_request).
      permit(:hat, :link))
    @hat_request.approve_by_user!(@user)

    flash[:success] = "Successfully approved hat request."

    return redirect_to "/hats/requests"
  end

  def reject_request
    @hat_request = HatRequest.find(params[:id])
    @hat_request.reject_by_user_for_reason!(@user,
      params[:hat_request][:rejection_comment])

    flash[:success] = "Successfully rejected hat request."

    return redirect_to "/hats/requests"
  end
end
