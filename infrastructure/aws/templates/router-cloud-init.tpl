#cloud-config
write_files:
-   encoding: b64
    path:  /home/ubuntu/router/application-production.properties
    content: "${router_service_properties}"
    permissions: '0644'
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/influx.conf
    content: "${influx_config}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - nodejs

runcmd:
  - sed -i "s/MY_HOSTNAME/JC_RouterSrv_$HOSTNAME/g" /etc/telegraf/telegraf.d/influx.conf
  - wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
  - sudo dpkg -i telegraf_1.6.3-1_amd64.deb
  - mkdir -p /home/ubuntu/router
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_service_tar}" /home/ubuntu/router/router.tgz
  - tar xzf /home/ubuntu/router/router.tgz -C /home/ubuntu/router
  - cd /home/ubuntu/router/m2m.router && LOBSTER_DNS_NAME=http://${lobster_service_lb}:80 MODLOG_DNS_NAME=${modlog_service_lb} ZIPKIN_SERVICE_URL=${zipkin_hostname} node index.js