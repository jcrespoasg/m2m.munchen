package com.n6consulting.m2m.request;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
interface RequestRepository extends PagingAndSortingRepository<Request, Long> {
    Iterable<Request> findByStatus(Request.Status status);
}
