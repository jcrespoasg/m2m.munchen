package com.n6consulting.m2m.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/request")
public class RequestController {
  @Autowired
  private RequestRepository requestRepository;

  static Resource<Request> createResource(Request req) {
    Resource<Request> res = new Resource<>(req);
    res.add(linkTo(methodOn(RequestController.class).getById(req.getRequestId()))
          .withSelfRel());
    addActionLinks(res);
    return res;
  }

  @RequestMapping(method = RequestMethod.GET)
  Resources<Resource<Request>> index() {
    final Iterable<Request> all = requestRepository.findAll();

    List<Resource<Request>> allRes =
            StreamSupport.stream(all.spliterator(), false)
                    .map(RequestController::createResource)
                    .collect(Collectors.toList());

    return new Resources<>(allRes);
  }

  @RequestMapping(method = RequestMethod.POST)
  Resource<Request> add(@RequestBody Request input) {
    for (Evidence e : input.getEvidence()) {
      e.setRequest(input);
    }
    return createResource(requestRepository.save(input));
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  ResponseEntity<Resource<Request>> getById(@PathVariable Long id) {
    Optional<Request> v = requestRepository.findById(id);

    return v.map(RequestController::createResource)
            .flatMap(res -> Optional.of(ResponseEntity.ok(res)))
            .orElse(ResponseEntity.notFound().build());
  }

  @RequestMapping(path = "status", method = RequestMethod.GET)
  Resources<Resource<Request>> findByStatus(@RequestParam("status") Request.Status param) {
    Iterable<Request> all = requestRepository.findByStatus(param);

    List<Resource<Request>> allRes =
            StreamSupport.stream(all.spliterator(), false)
                    .map(RequestController::createResource)
                    .collect(Collectors.toList());

    return new Resources<>(allRes);
  }

  enum Action implements Identifiable<String> {
    approve(Request.Status.approved),
    reject(Request.Status.rejected);

    private Request.Status targetStatus;

    Action(Request.Status status) {
      this.targetStatus = status;
    }

    Request doTo(Request r) {
      r.setStatus(targetStatus);
      return r;
    }

    @Override
    public String getId() {
      return this.name();
    }
  }

  @RequestMapping(path = "/{id}/{action}", method = RequestMethod.POST)
  ResponseEntity<Resource<Request>> action(@PathVariable(name = "id") Long id,
         @PathVariable("action") Action action, 
         @RequestBody Notarization notarization) {
    Optional<Request> request = requestRepository.findById(id);

    return request.filter(Request::isOpen)
            .map(action::doTo)
            .map(r -> r.notarize(notarization))
            .map(requestRepository::save)
            .map(RequestController::createResource)
            .map(r -> ResponseEntity.ok(r))
            .orElse(ResponseEntity.notFound().build());
  }

  private static void addActionLinks(Resource<Request> resource) {
    if (resource.getContent().isOpen()) {
      for (Action a : Action.values()) {
        resource.add(actionLink(a, resource));
      }
    }
  }

  private static Link actionLink(Action action, Resource<Request> resource) {
    return linkTo(methodOn(RequestController.class).action(resource.getContent().getRequestId(), action, null)).withRel(action.name());
  }
}
